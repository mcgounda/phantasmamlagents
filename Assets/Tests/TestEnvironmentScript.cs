using System.Collections;
using System.Linq;
using NUnit.Framework;
using Phantasma;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

public class TestEnvironmentScript
{
    private EnvironmentScript env;
    private PhantasmaAgent agent;

    [SetUp]
    public void Setup()
    {
        var prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Environment.prefab");
        var go = Object.Instantiate(prefab);
        env = go.GetComponent<EnvironmentScript>();
        agent = env.GetComponentInChildren<PhantasmaAgent>();
    }

    [TearDown]
    public void Teardown()
    {
        Object.Destroy(env.gameObject);
    }

    [UnityTest]
    public IEnumerator TestSpawnGoals()
    {
        env.SpawnGoals();

        yield return new WaitForEndOfFrame();

        Assert.GreaterOrEqual(env.Goals.Count, 1);
        Assert.LessOrEqual(env.Goals.Count, 10);

        foreach (var goal1 in env.Goals.Values)
        {
            float distance = Vector3.Distance(goal1.transform.position, agent.transform.position);
            Assert.GreaterOrEqual(distance, 2f);

            foreach (var goal2 in env.Goals.Values)
            {
                if (goal1 == goal2)
                {
                    continue;
                }

                distance = Vector3.Distance(goal1.transform.position, goal2.transform.position);
                Assert.GreaterOrEqual(distance, 2f);
            }
        }
    }

    [UnityTest]
    public IEnumerator TestGoalReached()
    {
        int goalCount = env.Goals.Count;
        Assert.NotZero(goalCount);

        env.GoalReached(env.Goals.First().Value);
        yield return new WaitForEndOfFrame();

        Assert.AreEqual(goalCount - 1, env.Goals.Count);
    }
}
