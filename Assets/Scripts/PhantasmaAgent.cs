using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;

namespace Phantasma
{
    public class PhantasmaAgent : Agent
    {
        [SerializeField]
        private float speed = 1f;

        public Transform TargetGoal { get; set; }
        private EnvironmentScript env;

        private void Awake()
        {
            env = GetComponentInParent<EnvironmentScript>();
        }

        private void Update()
        {
            Debug.DrawLine(transform.position, TargetGoal.position, Color.magenta);
        }

        public override void OnEpisodeBegin()
        {
            transform.localPosition = new Vector3(Random.Range(-9, 9), 0, Random.Range(-9, 9));
            env.SpawnGoals();
        }

        public override void CollectObservations(VectorSensor sensor)
        {
            sensor.AddObservation(transform.localPosition);
            sensor.AddObservation(TargetGoal.localPosition);
        }

        public override void OnActionReceived(ActionBuffers actions)
        {
            float actionX = actions.ContinuousActions[0];
            float actionY = actions.ContinuousActions[1];
            transform.localPosition += new Vector3(actionX, 0, actionY) * speed * Time.deltaTime;
        }

        public override void Heuristic(in ActionBuffers actionsOut)
        {
            var continuousActions = actionsOut.ContinuousActions;
            continuousActions[0] = Input.GetAxis("Horizontal");
            continuousActions[1] = Input.GetAxis("Vertical");
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Goal")
            {
                SetReward(1f);
                env.GoalReached(other.gameObject);

                if (env.Goals.Count == 0)
                {
                    EndEpisode();
                }
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.tag == "Border")
            {
                SetReward(-0.1f);
            }
        }
    }
}