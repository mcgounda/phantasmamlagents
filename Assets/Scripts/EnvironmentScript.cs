using System.Collections.Generic;
using UnityEngine;

namespace Phantasma
{
    public class EnvironmentScript : MonoBehaviour
    {
        [SerializeField]
        private GameObject goalPrefab;

        public Dictionary<int, GameObject> Goals { get; set; } = new Dictionary<int, GameObject>();
        private PhantasmaAgent agent;

        private void Awake()
        {
            agent = GetComponentInChildren<PhantasmaAgent>();
        }

        private void Update()
        {
            UpdateNearestGoal();
        }

        /// <summary>
        /// Spawns a random (1 - 10) number of goals spaced at least 2 units apart from each other and the agent.
        /// </summary>
        public void SpawnGoals()
        {
            CleanupGoals();
            int random = Random.Range(1, 11);

            for (int i = 0; i < random; i++)
            {
                Vector3 startPos;
                bool validPos;

                do
                {
                    startPos = new Vector3(Random.Range(-9, 9), 0, Random.Range(-9, 9));
                    validPos = true;

                    float distance = Vector3.Distance(startPos, agent.transform.position);
                    if (distance < 2f)
                    {
                        validPos = false;
                        continue;
                    }

                    foreach (var goal in Goals.Values)
                    {
                        distance = Vector3.Distance(startPos, goal.transform.position);
                        if (distance < 2f)
                        {
                            validPos = false;
                            break;
                        }
                    }
                } while (!validPos);

                var go = Instantiate(goalPrefab, startPos, Quaternion.identity, transform);
                Goals.Add(go.GetInstanceID(), go);
            }

            UpdateNearestGoal();
        }

        /// <summary>
        /// Destroys the goal and if there are goals remaining, updates the nearest goal to the agent.
        /// </summary>
        /// <param name="goal">The <see cref="GameObject"/> of the goal that was triggered.</param>
        public void GoalReached(GameObject goal)
        {
            Destroy(goal);
            Goals.Remove(goal.GetInstanceID());

            if (Goals.Count > 0)
            {
                UpdateNearestGoal();
            }
        }

        private void CleanupGoals()
        {
            foreach (var goal in Goals.Values)
            {
                Destroy(goal);
            }
            Goals.Clear();
        }

        private void UpdateNearestGoal()
        {
            float minDistance = 100;
            Transform nearestGoal = null;

            foreach (var goal in Goals.Values)
            {
                float distance = Vector3.Distance(goal.transform.position, agent.transform.position);
    
                if (distance < minDistance)
                {
                    minDistance = distance;
                    nearestGoal = goal.transform;
                }
            }

            agent.TargetGoal = nearestGoal;
        }
    }
}