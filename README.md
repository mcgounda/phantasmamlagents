# Phantasma Task

## What was done

 - Created a basic environment for the agent to train in.
 - Created an environment script for spawning and clearing goals.
 - Created an ML Agent that tries to learn how to reach the goals.
 - Created unit tests to test the functionality of the goal spawning and cleanup.

### Environment Script

 - At the beginning of each training episode, it spawns 1-10 goals that are at least 2m from each other and the agent. It also cleans up any remaining goals from the previous episode.
 - When the agent reaches a goal, the script destroys the goal.
 - Every Update, the scripts updates the nearest goal to the agent.

### Agent

 - At the beginning of each training episode, it places the agent in a random position in the environment and calls the environment function to spawn the goals.
 - The agent observes the agent's position and the nearest goal's position. It also has ray perception sensors for the floor and border.
 - When an action is received, it moves the agent along the X/Z axis.
 - After reaching the goal, the reward is incremented by one and GoalReached is called in the environment script. If there are no goals left after the environment script destroys the goal, it will end the episode.
 - When colliding with the border the reward is decreased by 0,1.
